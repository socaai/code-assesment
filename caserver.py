from flask import Flask, json, render_template, request, jsonify
from bson.objectid import ObjectId
from bson import json_util
import pymongo, json, openai
import requests

app = Flask(__name__)

try:
    mongo = pymongo.MongoClient(host="localhost", 
    port=27017,
    serverSelectionTimeoutMS=1000
    )
    db = mongo.soal
    mongo.server_info() ##trigger exception to connection
except:
    print("Error gabisa connect ke db")

soal = db.code.find({"_id":ObjectId('6422de55f1ac73d71db326d7')})[0]

def compileCode(code, lang, inp=None):       
    data = {
        "code":'''%s'''%(code),
        'language':'%s'%(lang),
        'input':inp
    }
    r = requests.post(url = "https://api.codex.jaagrav.in/", json = data)
    # print(r.json())
    return r.json()


@app.get('/')
def display():
    # soal = db.code.find({"_id":ObjectId('641d42cb280b44166e87b3b9')})
    # # print(soal[0])
    return render_template('index.html', problem = soal["problem"], expected = soal["expected result"], examplein=soal["test case"][2]["input"], exampleout=soal["test case"][2]["output"])

@app.get("/getdata")
def getData():
    # soal = db.code.find({"_id":ObjectId('641d42cb280b44166e87b3b9')})
    return json.loads(json_util.dumps(soal))

@app.route("/compare", methods=["POST"])
def compare() :
    lang = request.form["lang"]
    code  = request.form["code"]
    count = 0
    number = 0
    result = {}
    for i in soal["test case"]:
        number+=1
        inp = i["input"]
        datacompile = compileCode(code, lang, inp)
        if datacompile["output"] == "" :
            return jsonify(datacompile["error"])
        if datacompile["output"].rstrip() == i["output"].rstrip():
            count+=1
        result["test%i"%number] = json.loads(''' {\"your result\" : "%s", \"expected result\" : "%s", \"error\":"%s"}'''%(datacompile["output"].rstrip(), i["output"].rstrip(), datacompile["error"]))
    result["Scores"]= "%i/%i"%(count, len(soal["test case"]))
    
    return jsonify(result)
    


app.run(debug=True)