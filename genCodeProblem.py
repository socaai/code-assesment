import pymongo, openai, json, tiktoken
# from codeXapi import compileCode


# Get Question
def countToken(string:str, encoding_:str):
    encoding = tiktoken.get_encoding(encoding_)    
    num_tokens = len(encoding.encode(string))
    return 4001-num_tokens

try:
    mongo = pymongo.MongoClient(host="localhost", 
    port=27017,
    serverSelectionTimeoutMS=1000
    )
    db = mongo.soal
    mongo.server_info() ##trigger exception to connection
except:
    print("Error gabisa connect ke db")
api_key = "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"
openai.api_key = api_key



def classify(prompt):
    string = '''[{"command" : "%s", "tingkat pendidikan": null, "tingkat kelas" : null, "topik":null,"subtopik":null, "jumlah soal":null}] fill all the null columns, based on the provided command dalam bentuk json'''%(prompt)
    classification = openai.Completion.create(
        model="text-davinci-003",
        prompt='''[{"command" : "%s", "topik":null,"subtopik":null}] fill all the null columns, based on the provided command dalam bentuk json'''%(prompt),
        max_tokens=countToken(string,"gpt2")
    )
    return classification['choices'][0]['text']


cmd = '''buatkan soal pemrograman dengan topik stack dalam format {"problem":null,"expected_result":null,"test_case":null}. field expected_result berisi detail result yang diharapkan sesuai problem. field test_case berisi array dengan panjang 10 dan setiap row memiliki field "input" yang berisi data yang diinputkan sebagai test case dan field "output" yang berisi output yang dihasilkan sesuai input dan setiap line break diubah menjadi \n. setiap test case memiliki input dan output yang berbeda:'''
# cmd2 ='''buatkan soal problem pemrograman tentang %s %s dalam bentuk json dengan key 'problem' yang berisi penjelasan masalah lengkap, key 'expected result' yang menjelaskan format output yang diterima, dan key'test case' yang berisi key 'input' yaitu data input sederhana hanya value yang dipisahkan spasi dan apabila input memiliki lebih dari 1 baris input awal harus , dan 'output' yang berisi output hanya value yang disatukan dalam bentuk string dari program yang berjumlah 10 buah test case'''%(classified_prompt['topik'],classified_prompt['subtopik'])
def getQuestion(classified_prompt):
    
    chat = openai.ChatCompletion.create(
        model = "gpt-3.5-turbo",
        messages = [
            {"role":"system", "content":"json only"},
            {"role":"user", "content" : '''buatkan soal problem pemrograman tentang %s %s dalam bentuk json dengan key 'problem' yang berisi penjelasan masalah lengkap, key 'expected result' yang menjelaskan format output yang diterima, dan key'test case' yang berisi key 'input' yaitu data input sederhana hanya value yang dipisahkan spasi dan apabila input memiliki lebih dari 1 baris input awal harus menyertakan jumlah input, dan 'output' yang berisi output hanya value yang disatukan dalam bentuk string dari program yang berjumlah 10 buah test case'''%(classified_prompt['topik'],classified_prompt['subtopik'])}
        ]
    )
    # chat = openai.Completion.create(
    #     model = "text-davinci-003",
    #     prompt = cmd,
    #     max_tokens = countToken(cmd,'gpt2')
    # )
    return chat['choices'][0]['message']['content']
    # return chat['choices'][0]['text']



# print(compileCode(code="print('hello world)", lang="py"))
respclass = classify("buatkan soal pemrograman tentang algoritma dan struktur data stack")
respclass = respclass.replace("[","").replace("]", "").strip()
respclass = json.loads(respclass)
print(respclass)
Question = getQuestion(respclass)
print(Question)


db.code.insert_one(json.loads(Question))